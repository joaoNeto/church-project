/* eslint-disable */
import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  state: {
    accessToken: undefined,
    httpRequesting: false,
  },
  mutations: {
    setAccessToken(state, value) {
      state.accessToken = value;
    },
    setHttpRequesting(state, value) {
      state.httpRequesting = value;
    },
  },
  actions: {
    async login({ commit }, request) {
      commit('setHttpRequesting', true);
      const query = 'mutation { login(email: "' + request['email'] + '", password: "' + request['password'] + '") }';
      const response = await Vue.prototype.$http.post('', { query });
      const errors = response?.data?.errors;

      if(errors) {
        alert(errors[0]['debugMessage']);
      }
      
      commit('setAccessToken', response?.data?.data?.login);
      commit('setHttpRequesting', false);
    }
  },
  plugins: [vuexLocal.plugin],
});
