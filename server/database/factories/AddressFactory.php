<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'postcode' => $this->faker->postcode(),
            'country' => $this->faker->country(),
            'state' => 'Pernambuco',
            'city' => $this->faker->city(),
            'street_name' => $this->faker->streetName(),
            'street_number' => $this->faker->buildingNumber()
        ];
    }
}
