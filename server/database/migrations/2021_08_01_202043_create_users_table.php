<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->string('phone');
            $table->enum('profile', ['pastor', 'member']);
            $table->string('password');
            $table->unsignedBigInteger('address_id');
            $table->unsignedBigInteger('church_id');
            $table->timestamps();
            $table->softDeletes();
            $table->rememberToken();

            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('church_id')->references('id')->on('churches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
