<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Church;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    private $numberOfChurches = 10;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Address::factory($this->numberOfChurches)->create()->each(function ($address) {
            Church::factory()->create([
                'address_id' => $address->id
            ])->each(function ($church) use ($address) {

                $numberOfChurchMembers = rand(10,20);

                // creating pastors
                User::factory()->create([
                    'address_id' => $address->id,
                    'church_id' => $church->id,
                    'profile' => User::PASTOR_PROFILE
                ]);
                
                // creating members 
                Address::factory($numberOfChurchMembers)->create()->each(function ($address) use ($church) {
                    User::factory()->create([
                        'address_id' => $address->id,
                        'church_id' => $church->id,
                        'profile' => User::MEMBER_PROFILE
                    ]);    
                });

            });
        });
    }
}
