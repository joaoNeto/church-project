<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

class Login
{
    public function __invoke($_, array $request)
    {
        if (!$token = JWTAuth::attempt($request)) {
            throw new Exception("Unauthorized, please check the data are correct", Response::HTTP_UNAUTHORIZED);
        }

        if(auth()->user()->profile != User::PASTOR_PROFILE) {
            throw new Exception("Unauthorized, only pastors can log in", Response::HTTP_UNAUTHORIZED);
        }

        return $token;
    }
}
