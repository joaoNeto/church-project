<?php

namespace App\GraphQL\Mutations;

use App\Models\Address;
use App\Models\Church;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class CreateChurch
{
    public function __invoke($_, array $request)
    {

        DB::beginTransaction();
        try{

            $address = Address::create($request);

            $request['address_id'] = $address->id;

            $church = Church::create([
                'name' => $request['church_name'],
                'address_id' => $request['address_id']
            ]);

            $request['church_id'] = $church->id;
            $request['password'] = Hash::make($request['password']);
            $request['profile'] = User::PASTOR_PROFILE;

            $user = User::create($request);

            DB::commit();

            return JWTAuth::fromUser($user);
        } catch(Exception $ex) {
            DB::rollBack();
            throw $ex;
        }
    }
}
