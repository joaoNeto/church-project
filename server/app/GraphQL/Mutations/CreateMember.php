<?php

namespace App\GraphQL\Mutations;

use App\Models\Address;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateMember
{
    public function __invoke($_, array $request)
    {

        DB::beginTransaction();
        try{

            $address = Address::create($request);

            $request['address_id'] = $address->id;
            $request['church_id'] = auth()->user()->church_id;
            $request['password'] = Hash::make($request['password']);
            $request['profile'] = User::MEMBER_PROFILE;

            $user = User::create($request);

            DB::commit();

            return true;
        } catch(Exception $ex) {
            DB::rollBack();
            throw $ex;
        }        
    }
}
