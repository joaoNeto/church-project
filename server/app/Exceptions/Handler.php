<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {

        if($exception instanceof ValidationException) {
            return response()->json(["message" => "invalid data", 'errors' => $exception->errors()], Response::HTTP_BAD_REQUEST);
        }

        if($exception instanceof ModelNotFoundException) {
            return response()->json(["message" => "not found resource"], Response::HTTP_NOT_FOUND);
        }

        if($exception instanceof QueryException) {
            return response()->json(["message" => "internal error. ".$exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $code = $exception->getCode();

        if($code == 0) {
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return response()->json(["message" => $exception->getMessage()], $code);
    }

}
